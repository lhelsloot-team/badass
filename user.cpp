#include "MurmurHash3.h"
#include "user.hpp"
#include "config.hpp"
#include "universal_reencryption.hpp"

user::additive_shares_t user::share() const
{
    auto s = runtime::get().shamir;
    auto d = runtime::get().profile_d;

    std::vector<shamir::value_t> xa;
    std::vector<shamir::value_t> xb;
    xa.reserve(d);
    xb.reserve(d);

    for (size_t i = 0; i < d; i++)
    {
        //xb.push_back(shamir::to_value(RandomProvider::GetInstance().GetRandomInteger(BigInteger(p))));
        shamir::value_t r = s->randomizer();
        shamir::value_t v = this->x[i] - r;
        if (v < 0)
        {
            v += shamir::p;
        }
        xb.push_back(r);
        xa.push_back(v);
    }

    return std::make_pair(std::move(xa), std::move(xb));
}

user_profile_share::user_profile_share(user_info const& user)
    : complete(false), info(user)
{
    x.reserve(runtime::get().profile_d);
}


user_factory::user_factory()
#ifndef NO_CRYPTO
    : crypto_provider(PlainElGamal::ConstructFromConfig())
{
    //crypto_provider->GenerateKeys();
    auto& pk = crypto_provider->GetPublicKey();
    ure_set_params(pk.p, pk.q);
}
#else
{}
#endif

user user_factory::get_user(ahead::DataLine const& data)
{
    user u(data.user_id, this->crypto_provider);

    for (auto const& element : data.profile_data)
    {
        uint32_t out;
        std::string value = element.first + element.second;

        MurmurHash3_x86_32(value.c_str(), value.length(), 42, &out);

        out %= runtime::get().profile_d;

        u.x[out] = u.x[out] + 1;
    }

    return u;
}
