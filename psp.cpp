#include "psp.hpp"
#include "universal_reencryption.hpp"
#include "timing.hpp"

/*
psp::psp()
    : crypto_provider(std::make_shared<ahead::ThresholdPaillier>())
{}

ahead::ThresholdPaillier::PrivateKeyShare psp::generate_key_shares()
{
    auto share = this->crypto_provider->GenerateKeyShares();
    this->public_crypto = std::make_shared<ahead::ThresholdPaillier>(this->get_public_key());
    return share;
}

SeComLib::Core::PaillierPublicKey const& psp::get_public_key() const
{
    return this->crypto_provider->GetPublicKey();
}
*/

void psp::calculate_sigma(dsp_group& dsps)
{
    shamir::value_t s = dsps.combine(runtime::get().shamir->multiply_threshold);

    double x = fp::to_double(s);
    double sigma = 1. / (1. + std::exp(-std::max(std::min(x, 20.), -20.)));

    //std::cout << "x: " << x << ", σ: " << sigma << std::endl;

    dsps.split(fp::to_fp(sigma));
}

void psp::submit_bid(shamir::index_t r_a, bid_response const& bid)
{
    this->bids[bid.bid_id].insert(std::make_pair(r_a, bid));
}

bid_response psp::get_ad(unsigned bid_id, dsp_group& comparison_group)
{
    unsigned long r_a = shamir::to_number(comparison_group.get_result(1));

    // Lookup ad
    bid_response result = this->bids.at(bid_id).at(r_a);
    this->bids.erase(bid_id);

#ifndef NO_CRYPTO
    ure_reencrypt(result.enc_ad);
#endif

    return result;
}

void psp::submit_mix_shares (dsp::shares_vector_t&& gradients, dsp::share_deque_t&& k, dsp::share_deque_t&& b)
{
    this->shares_to_mix.emplace_back(std::make_tuple(std::move(gradients), std::move(k), std::move(b)));
}

dsp::update_shares_array_t allocate_split_gradient()
{
    size_t profile_d = runtime::get().profile_d;
    size_t n_gradients = runtime::get().dsp_update_threshold;

    dsp::update_shares_array_t split_gradients;

    // Allocate required sizes
    for (auto& dest_vector : split_gradients)
    {
        //dest_vector.resize(n_shares);
        for (auto& source_vector : dest_vector)
        {
            source_vector.resize(n_gradients);
            for (auto& g : source_vector)
            {
                g.reserve(profile_d);
            }
        }
    }

    return split_gradients;
}

// split_k[dest DSP][source DSP][k]
dsp::inverted_shares_array_t allocate_split()
{
    size_t profile_d = runtime::get().profile_d;
    size_t n_gradients = runtime::get().dsp_update_threshold;

    dsp::inverted_shares_array_t v;
    for (auto& el : v)
    {
        //el.resize(n_shares);
        for (auto& el2 : el)
        {
            el2.resize(n_gradients);
        }
    }
    return v;
}

psp::psp()
    : split_gradients(allocate_split_gradient())
    , split_k(allocate_split())
    , split_b(allocate_split()) {}

static Timer<timer_id::model_update_mix, true> model_timer("update_model_mix");
void psp::mix_shares(dsp_group& group)
{
    model_timer.start();
    auto s = runtime::get().shamir;

    size_t n_gradients = std::get<0>(this->shares_to_mix.front()).size();
    size_t n_shares = this->shares_to_mix.size();

    // Re-share gradient vectors
    size_t profile_d = runtime::get().profile_d;

    // Randomly rotate all received shares
    //size_t r = RandomProvider::GetInstance().GetRandomInteger(BigInteger(runtime::get().dsp_update_threshold)).ToUnsignedLong();

    //std::uniform_int_distribution<size_t> dis(0, runtime::get().dsp_update_threshold - 1);
    //size_t r = dis(runtime::get().shamir->random_dev);
    size_t r = s->random.get(n_gradients - 1);

    for (auto& shares_pair : this->shares_to_mix)
    {
        std::rotate(std::get<0>(shares_pair).begin(), std::get<0>(shares_pair).begin() + r, std::get<0>(shares_pair).end());
        std::rotate(std::get<1>(shares_pair).begin(), std::get<1>(shares_pair).begin() + r, std::get<1>(shares_pair).end());
        std::rotate(std::get<2>(shares_pair).begin(), std::get<2>(shares_pair).begin() + r, std::get<2>(shares_pair).end());
    }

    // Re-share rotated gradient vectors
    // This is where the fun starts!

    // split_gradients[dest DSP][source DSP][gradient][i]
    //static auto split_gradients = allocate_split_gradient(profile_d, n_gradients, n_shares);

    // split_k[dest DSP][source DSP][gradient]
    //static auto split_k = allocate_split(n_gradients, n_shares);
    //static auto split_b = allocate_split(n_gradients, n_shares);

    for (size_t source = 0; source < n_shares; source++)
    {
        auto& source_gradients = std::get<0>(this->shares_to_mix[source]);
        auto& k_vector = std::get<1>(this->shares_to_mix[source]);
        auto& b_vector = std::get<2>(this->shares_to_mix[source]);
        for (unsigned gradient = 0; gradient < n_gradients; gradient++)
        {
            auto& gradient_vector = source_gradients[gradient];
            for (unsigned i = 0; i < profile_d; i++)
            {
                shamir::shares_t split_gradient = s->reduce_degree_split(gradient_vector[i]);
                for (unsigned dest = 0; dest < n_shares; dest++)
                {
                    split_gradients[dest][source][gradient][i] = split_gradient[dest];
                }
            }

            shamir::shares_t k_shares = s->reduce_degree_split(k_vector[gradient]);
            shamir::shares_t b_shares = s->reduce_degree_split(b_vector[gradient]);
            for (unsigned dest = 0; dest < n_shares; dest++)
            {
                split_k[dest][source][gradient] = k_shares[dest];
                split_b[dest][source][gradient] = b_shares[dest];
            }
        }
    }

    model_timer.stop();

    group.update_model(split_gradients, split_k, split_b);

    // Invoke update-model at DSPs
    /*for (size_t i = 0; i < n_shares; i++)
    {
        auto d = std::dynamic_pointer_cast<dsp>(group.parties.at(i));
        d->update_model(split_gradients.at(i), std::get<1>(this->shares_to_mix.at(i)), dec_k.at(i), split_k.at(i), split_b.at(i));
    }*/

    this->shares_to_mix.clear();
}

void psp::report_logloss(double loss, unsigned long k)
{
    logloss[k] -= loss;
    count[k]++;
}
