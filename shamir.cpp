#include <numeric>
#include <random>

#include "shamir.hpp"
#include "secomlib.hpp"
#include "random_pool.hpp"

namespace shamir
{

numeric_value_t mod(int64_t a, numeric_value_t b)
{
    /*if (b < 0)
    {
        return mod(a, -b);
    }*/

    int64_t ret = a % b;
    if (ret < 0)
    {
        ret += b;
    }

    return ret;
}

BigInteger mod(BigInteger const& a, BigInteger const& b)
{
    return a % b;
}

share_t::share_t(value_t const& y_arg)
    : y (y_arg) {}

void share_t::check_transform(share_t const& other) const
{
    /*if (this->x != other.x)
    {
        throw std::invalid_argument("x coordinates of shares to add do not match");
    }*/

    /*if (this->p != other.p)
    {
        throw std::invalid_argument("shares to add must be in the same group");
    }*/
}


share_vector& operator+= (share_vector& lhs, share_vector const& rhs)
{
    size_t length = lhs.size();
    /*if (rhs.size() != length)
    {
        throw std::invalid_argument("Lenghts of share vectors to add must match");
    }*/

    for (size_t i = 0; i < length; i++)
    {
        lhs[i] += rhs[i];
    }

    return lhs;
}

share_vector& operator-= (share_vector& lhs, share_vector const& rhs)
{
    size_t length = lhs.size();
    /*if (rhs.size() != length)
    {
        throw std::invalid_argument("Lenghts of share vectors to add must match");
    }*/

    for (size_t i = 0; i < length; i++)
    {
        lhs[i] -= rhs[i];
    }

    return lhs;
}

polynomial_t::polynomial_t(shamir* s_arg)
    : s(s_arg)
{
    this->poly.reserve(s->t);
}

void polynomial_t::generate(const value_t& v)
{
    //this->poly.clear();

    // f(0) is our secret
    this->poly[0] = v;

    // Pick random coefficients
    for (index_t i = 1; i < s->t; i++)
    {
        value_t r;
        //do
        //{
            r = this->s->randomizer();
        //} while (r == 0);

        this->poly[i] = r;
    }
}

share_t polynomial_t::operator()(index_t x) const
{
    int64_t y = this->poly[0];
    int64_t exp = 1;
    for (index_t i = 1; i < s->t; i++)
    {
        exp = (exp * x);
        int64_t term = (this->poly[i] * exp);
        y = (y + term);
    }

    return share_t(y % p);
}


shamir::shamir(index_t t_arg, index_t n_arg, size_t l_arg, size_t k_arg)
    : t (t_arg)
    , n (n_arg)
    , l (l_arg)
    , k (k_arg)
    , multiply_threshold((t-1) * 2 + 1)
    , poly_cache(this)
{
    this->random.fill_default_pool(std::uniform_int_distribution<value_t>(1, p-1));
}

value_t shamir::randomizer()
{
    return this->random.get();
}

shares_t shamir::split(value_t const& v)
{
    shares_t points;

    poly_cache.generate(v);

    for (index_t x = 1; x <= this->n; x++)
    {
        points[x-1] = poly_cache(x);
    }

    return points;
}

long long calcInverse(long long  a, long long b)
{
    int b0 = b, t, q;
    int x0 = 0, x1 = 1;
    if (b == 1) return 1;
    while (a > 1) {
        q = a / b;
        t = b, b = a % b, a = t;
        t = x0, x0 = x1 - q * x0, x1 = t;
    }
    if (x1 < 0) x1 += b0;
    return x1;
}


value_t mod_pow(uint64_t num, uint64_t pow, uint64_t mod)
{
    uint64_t test;
    for(test = 1; pow; pow >>= 1)
    {
        if (pow & 1)
            test = (test * num) % mod;
        num = (num * num) % mod;
    }

    return test;

}

value_t shamir::combine(shares_t const& shares, index_t n_shares) const
{
    uint64_t f_x = 0;

    /*if (!n_shares)
    {
        n_shares = this->t;
    }*/

    /*if (shares.size() < n_shares)
    {
        throw std::invalid_argument("Insufficient number of shares to reconstruct secret");
    }*/

    for (size_t j = 1; j <= n_shares; j++)
    {
        int64_t numerator = 1;
        int64_t denominator = 1;

        for (size_t m = 1; m <= n_shares; m++)
        {
            if (j == m)
            {
                continue;
            }

            numerator = numerator * m;
            denominator = denominator * (static_cast<long long>(m) - (j));
        }

        denominator = mod(denominator, p);
        uint64_t lagrange_polynomial = (calcInverse(denominator, p) * numerator) % p;

        share_t const& sj = shares[j-1];
        f_x = f_x + (sj.y * lagrange_polynomial);
    }

    return f_x % p;
}

#if false
share_t shamir::reduce_degree_combine2(shares_t const& shares) const
{
    if (shares.empty())
    {
        throw std::invalid_argument("Cannot reduce degree with 0 shares");
    }

    share_t first = shares[0];

    index_t x = first.x;
    value_t sum = 0;
    value_t p = first.p;

    for (auto const& share : shares)
    {
        if (share.x != x)
        {
            throw std::invalid_argument("Cannot reduce degree with different x coordinates");
        }

        if (share.p != p)
        {
            throw std::invalid_argument("Cannot reduce degree with different groups");
        }

        std::cout << "Y: " << share.y.ToString(10) << std::endl;

        sum = (sum + share.y) % p;
    }

    std::cout << sum.ToString(10) << std::endl;

    return share_t(x, sum, p);
}
#endif

share_t shamir::reduce_degree_combine(shares_t& shares) const
{
    //if (shares.empty())
    //{
    //    throw std::invalid_argument("Cannot reduce degree with 0 shares");
    //}

    //shares_t adjusted_shares;
    //for (index_t i = 0; i < shares.size(); i++)
    //{
    //    adjusted_shares.emplace_back(i + 1, shares[i].y, shares[i].p);
    //}

    //index_t real_index = shares[0].x;
    /*for (index_t i = 0; i < shares.size(); i++)
    {
        shares[i].x = i + 1;
    }*/

    return share_t(this->combine(shares, this->multiply_threshold));
}

value_t shamir::sqrt(value_t const& v) const
{
    /*if (this->p % 4 != 3)
    {
        throw std::invalid_argument("Cannot compute root with non-Blum modulus");
    }*/

    //return to_value(BigInteger(v).GetPowModN((this->p_bigint + 1) / 4, this->p_bigint));
    //uint64_t exp = p;
    //exp = (exp + 1) / 4;
    const uint64_t exp = (static_cast<uint64_t>(p) + 1) / 4;
    return mod_pow(v, exp, p);
}

party::party(std::shared_ptr<shamir> s_arg, index_t x_arg)
    : s(s_arg), x(x_arg)
{
    this->stack.reserve(16);
}

void party::push(share_t const& value)
{
    this->stack.push_back(value);
}

void party::pop()
{
    check_stack(1);
    this->stack.pop_back();
}

share_t& party::top()
{
    check_stack(1);
    return this->stack.back();
}

share_t party::pop_top()
{
    share_t v = top();
    pop();
    return v;
}

share_t& party::at(size_t n)
{
    return this->stack.at(this->stack.size() - n - 1);
}

void party::dup()
{
    this->stack.push_back(this->top());
}

void party::swap(int const& n1, int const& n2)
{
    std::swap(this->at(n1), this->at(n2));
}

void party::gte_calculate_z()
{
    // STACK: r r' b a

    check_stack(4);

    share_t r = this->pop_top();
    share_t r_mod2l = this->pop_top();

    // b' = 2b
    share_t& b = this->at(0);
    b *= 2;

    // a' = 2a + 1
    share_t& a = this->at(1);
    a *= 2;
    a += 1;

    // z = a' - b' + 2^l
    this->subtract();
    this->add(1 << s->l);
    // STACK: z

    share_t& z = this->top();

    this->push(r_mod2l);
    // STACK: r_mod2l z

    // c = z + r
    this->push(r);
    this->push(z);
    this->add();
    // STACK: c r_mod2l z
}

void party::gte_calculate_e(value_t const& c)
{
    // STACK: mask s_sign s_bit r_mod2l z
    check_stack(5);
    share_t& mask = this->top();
    share_t& s_sign = this->at(1);

    std::deque<share_t> sumXORs;
    sumXORs.emplace_front(0);
    for (int i = s->l - 2; i >= 0; i--)
    {
        share_t sum = sumXORs.front() + (temp_stack.at(i+1) ^ get_bit(c, i+1));
        sumXORs.push_front(sum);
    }

    std::deque<share_t> e_tilde;
    for (int i = 0; i < temp_stack.size(); i++)
    {
        share_t e_i = s_sign + (temp_stack.at(i) - get_bit(c, i));
        e_i += sumXORs[i] * 3;
        e_tilde.push_back(e_i);
    }

    // Replace temp_stack contents with e_tilde and mask
    this->temp_stack = std::move(e_tilde);
    this->temp_stack.push_back(mask);

    // Don't need mask and s_sign anymore
    this->pop();
    this->pop();
    // STACK: s_bit r_mod2l z
}

bool party::gte_can_multiply()
{
    return this->temp_stack.size() > 1;
}

void party::gte_multiply()
{
    this->push(this->temp_stack.front());
    this->temp_stack.pop_front();
    this->push(this->temp_stack.front());
    this->temp_stack.pop_front();
}

void party::gte_multiply_push_result()
{
    if (!this->temp_stack.empty())
    {
        this->temp_stack.push_back(this->pop_top());
    }
}

void party::gte_adjust(value_t const& c_mod2l, value_t const& inv2l)
{
    // result = (c_mod2l - r_mod2l) + (uf*2^l)
    this->multiply(1 << s->l); // uf * 2^l
    share_t& r_mod2l = this->at(1);
    r_mod2l *= -1;
    r_mod2l += c_mod2l;
    this->add();

    // z - result * 2^-l
    this->subtract();
    this->multiply(inv2l);
}

group::group(std::shared_ptr<shamir> s_arg)
    : s(s_arg)
{
    this->preproc();
}

void group::add_party(std::shared_ptr<party> party)
{
    this->parties.push_back(party);
}

void group::split(value_t const& value)
{
    check_group_size();

    shares_t shares = this->s->split(value);

    for (size_t i = 0; i < shares.size(); i++)
    {
        this->parties[i]->push(shares[i]);
    }
}

value_t group::combine(index_t n_shares)
{
    shares_t shares;
    for (size_t i = 0; i < shares.size(); i++)
    {
        auto& party = this->parties[i];
        party->check_stack(1);
        shares[i] = party->pop_top();
    }

    return this->s->combine(shares, n_shares);
}

void group::reduce_degree()
{
    this->reshare(*this);
}

void group::reshare(group& destination)
{
    std::array<shares_t, 5> subshares;

    //for (auto& v : subshares)
    //{
    //    v.resize(5);
    //}

    size_t j = 0;
    for (auto& party : this->parties)
    {
        shares_t split = destination.s->reduce_degree_split(party->pop_top());

        for (size_t i = 0; i < split.size(); i++)
        {
            subshares[i][j] = split[i];
        }
        j++;
    }

    for (size_t i = 0; i < subshares.size(); i++)
    {
        destination.parties[i]->push(destination.s->reduce_degree_combine(subshares[i]));
    }
}


shares_t group::reshare(group& destination, shares_t const& shares)
{
    std::array<shares_t, 5> subshares;

    size_t j = 0;
    for (auto& share : shares)
    {
        shares_t split = destination.s->reduce_degree_split(share);

        for (size_t i = 0; i < split.size(); i++)
        {
            subshares[i][j] = split[i];
        }
        j++;
    }

    shares_t result;
    for (size_t i = 0; i < subshares.size(); i++)
    {
        result[i] = destination.s->reduce_degree_combine(subshares[i]);
    }

    return result;
}

void group::multiply()
{
    for (auto& party : this->parties)
    {
        party->multiply();
    }

    this->reduce_degree();
}

void group::random_element()
{
    // Let each party split a random value
    for (auto& party : this->parties)
    {
        //value_t r = to_value(RandomProvider::GetInstance().GetRandomInteger(s->p_bigint));
        value_t r = this->s->randomizer();
        this->split(r);
    }

    // Calculate the sum of the split values
    for (size_t i = 0; i < this->parties.size() - 1; i++)
    {
        this->add();
    }
}

void group::random_nonzero_element()
{
    value_t result;
    do
    {
        // Push r onto stack
        this->random_element();

        // Duplicate r for 0 check
        this->call(&party::dup);

        // Push s onto stack
        this->random_element();

        // Multiply r and s
        // Degree reduction is not necessary, since we combine right away
        this->multiply_raw();

        // Open result, and check that it's nonzero
        result = this->combine(this->s->multiply_threshold);

        // If the result is 0, pop r and try again
        if (result == 0)
        {
            this->call(&party::pop);
        }
    } while (result == 0);
}

void group::random_bit()
{
    value_t rsquare;

    do
    {
        // Push r onto stack
        this->random_element();

        // Duplicate r twice for square
        this->call(&party::dup);
        this->call(&party::dup);

        // Multiply to calculate square
        // Degree reduction is not necessary, since we combine right away
        this->multiply_raw();

        // Open result = r^2
        rsquare = this->combine(this->s->multiply_threshold);

        // If the result is 0, pop r and try again
        if (rsquare == 0)
        {
            this->call(&party::pop);
        }
    } while (rsquare == 0);

    value_t binv = this->s->sqrt(rsquare);
    binv = calcInverse(binv, p);

    //BigInteger binv(this->s->sqrt(rsquare));
    //binv.InvertModN(this->s->p_bigint);

    // Multiply random value with b inverse, and add 1
    this->multiply(binv);
    this->add(1);

    // Multiply with 2 inverse to get result


    //BigInteger twoinv(2);
    //twoinv.InvertModN(this->s->p_bigint);

    this->multiply(this->twoinv);
}

void group::random_m(size_t m)
{
    // Clear temp stacks. They should be empty, but you never know...
    this->call(&party::clear_temp_stack);

    // Generate k + m random bits, and move them to the temp stack for later
    // processing
    for (size_t i = 0; i < s->k + m; i++)
    {
        this->random_bit();
        this->call(&party::move_to_temp_stack);
    }

    // Calculate r' % 2^m
    this->call(&party::push_zero);
    for (size_t i = 0; i < m; i++)
    {
        this->call(&party::copy_from_temp_stack, i);
        this->multiply(1 << i);
        this->add();
    }

    // Calculate r = 2^m r'' + r'
    this->call(&party::dup);

    for (size_t i = m; i < s->k + m; i++)
    {
        this->call(&party::copy_from_temp_stack, i);
        this->multiply(1 << i);
        this->add();
    }

    // Leave only the lower m bits on the temp stack
    this->call([](party& p) {
        for (size_t i = 0; i < p.s->k; i++)
        {
            p.temp_stack.pop_back();
        }
    });
}

void group::greater_than_equal()
{
    // Preprocessing

    // Clear temp stacks. They should be empty, but you never know...
    this->call(&party::clear_temp_stack);

    // Generate l + k random bits, and move them to the temp stack for later
    // processing
    /*for (size_t i = 0; i < s->l + s->k; i++)
    {
        this->random_bit();
        this->call(&party::move_to_temp_stack);
    }*/

    this->random_m(s->l);

    // STACK: b a
    this->call(&party::gte_calculate_z);
    // STACK: c r_mod2l z

    // Reveal c
    value_t c = this->combine();
    // STACK: r_mod2l z

    // Generate sign bit, store and turn into sign
    this->random_bit();
    // STACK: s_bit r_mod2l z

    this->call(&party::dup);
    this->multiply(-2);
    this->add(1);
    // STACK: s_sign s_bit r_mod2l z

    //this->inspect("s_sign");

    // Generate mask
    this->random_nonzero_element();
    // STACK: mask s_sign s_bit r_mod2l z

    this->call(&party::gte_calculate_e, c);
    // STACK: s_bit r_mod2l z

    // Perform log-rounds multiplication to determine whether there is a 0 in
    // E_tilde. This part is kind of hacky because results of multiplications
    // are pushed back to the bottom of the temp stack.
    // Basically, what happens is
    // while (E_tilde.size() > 1)
    // {
    //     E_tilde.push_back(E_tilde.pop_front() * E_tilde.pop_front())
    // }
    // The hacky part is that the multiplication results must be reduced on
    // group level, whereas the temp_stack operations must be performed on
    // party level.
    while (this->parties[0]->gte_can_multiply())
    {
        this->call(&party::gte_multiply);
        this->multiply();
        this->call(&party::gte_multiply_push_result);
    }
    // STACK: non_zero s_bit r_mod2l z

    // Reveal the result of the multiplication, and convert into a single bit.
    // This bit is the result of the comparison, XOR'ed with the hidden s_bit.
    value_t non_zero = this->combine();
    non_zero = non_zero != 0;
    // STACK: s_bit r_mod2l z

    // To get the result, xor the shared s_bit left on the stack with the public
    // non_zero bit. This leaves the result, [a < b], shared on the stack.
    this->exor(non_zero);
    // STACK: uf r_mod2l z

    //BigInteger factor(1 << s->l);
    //value_t inv2l = to_value(factor.GetInverseModN(this->s->p_bigint));

    value_t c_mod2l = c % (1 << s->l);

    this->call(&party::gte_adjust, c_mod2l, this->twolinv);
}

void group::truncate(size_t m)
{
    this->random_m(m);

    // STACK: r r' a

    // Move r and r' to temp stack
    this->call(&party::move_to_temp_stack);
    this->call(&party::move_to_temp_stack);

    // c = 2^(k-1) + [a] + r
    // Get r on stack and add
    this->call(&party::dup);
    this->call(&party::copy_from_temp_stack, static_cast<size_t>(1));
    this->add();
    this->add(1 << (m + s->k));

    // Open c
    value_t c = this->combine();
    value_t cprime = c % (1 << m);

    // STACK: a
    // [d] = ([a] - c' + [r'])(2^-m mod q)
    this->subtract(cprime);
    this->call(&party::copy_from_temp_stack, static_cast<size_t>(0));
    this->add();

    BigInteger factor(1 << m);
    //value_t inv2m = to_value(factor.GetInverseModN(this->s->p_bigint));
    value_t inv2m = calcInverse(1 << m, p);
    this->multiply(inv2m);
}

void group::check_group_size()
{
    if (this->s->n != this->parties.size())
    {
        throw std::invalid_argument("Number of parties in group is not equal to shamir::n");
    }
}

void group::preproc()
{
    //BigInteger twoinv(2);
    //twoinv.InvertModN(this->s->p_bigint);
    this->twoinv = calcInverse(2, p);
    this->twolinv = calcInverse(1 << this->s->l, p);
}

}
