#include "config.hpp"
#include "ad_exchange.hpp"
#include "psp.hpp"
#include "timing.hpp"


static Timer<timer_id::update_profile_psp, true> upd_timer("update_profile_psp");

void share_converter::share_profile(user_info const& user, std::vector<shamir::value_t> const& additive_shares, dsp_group const& dsps)
{
    upd_timer.start();
    shamir::ptr shamir = runtime::get().shamir;

    size_t n_dsps = dsps.parties.size();
    std::vector<user_profile_share> dsp_shares;
    dsp_shares.reserve(n_dsps);

    for (auto& dsp : dsps.parties)
    {
        dsp_shares.emplace_back(user);
    }

    for (auto& share : additive_shares)
    {
        shamir::shares_t value_shares = shamir->split(share);
        for (size_t i = 0; i < n_dsps; i++)
        {
            dsp_shares[i].x.push_back(value_shares[i]);
        }
    }
    upd_timer.stop();

    for (size_t i = 0; i < n_dsps; i++)
    {
        auto d = std::static_pointer_cast<dsp>(dsps.parties[i]);
        d->combine_profile(std::move(dsp_shares[i]));
    }
}

ad_exchange::ad_exchange(std::shared_ptr<psp> psp_arg)
    : m_psp(psp_arg) {}

void ad_exchange::add_dsp_group(std::shared_ptr<dsp_group> group)
{
    this->dsp_groups.insert(std::make_pair(group->id, group));
}


ahead::Timer<ahead::timer_id::bidding, true> bidding_timer("bidding");
ahead::Timer<ahead::timer_id::auction, true> auction_timer("auction");

bid_response ad_exchange::request_ad(user_info const& user)
{
    for (auto& item : this->dsp_groups)
    {
        bidding_timer.start();
        item.second->request_bids(next_bid_id, user);
        bidding_timer.stop();
    }


#ifdef MEASURE_RUNTIME
    auction_timer.start();
#endif
    // Pick a comparison group
    auto comp_group = this->dsp_groups.begin()->second;
    for (auto& item : this->dsp_groups)
    {
        item.second->submit_bids(*comp_group);
    }

    // Perform comparison
    comp_group->max(2);

    // Lookup ads at PSP
    bid_response result = this->m_psp->get_ad(next_bid_id, *comp_group);
#ifdef MEASURE_RUNTIME
    auction_timer.stop();
#endif

    next_bid_id++;

    return result;
}

void ad_exchange::submit_bid(shamir::index_t r_a, bid_response const& bid)
{
    this->m_psp->submit_bid(r_a, bid);
}
