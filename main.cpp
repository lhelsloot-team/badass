#include <iostream>
#include "user.hpp"
#include "config.hpp"
#include "ad_exchange.hpp"
#include "psp.hpp"
#include "timing.hpp"

#include "dataparser_avazu.hpp"

#include "universal_reencryption.hpp"
#include "elgamal.hpp"

int main(int argc, char **argv)
{
    /*auto s = runtime::get().shamir;

    for (size_t i = 0; i < 1000; i++)
    {
        auto v = s->split(i);
        auto c = s->combine(v);
        assert(c == i);
    }*/

    //g.split(3);
    //g.add();

    //g.inspect("8");

    /*return 0;*/


    /*PlainElGamal crypto;
    crypto.GenerateKeys();
    auto& pk = crypto.GetPublicKey();
    ure_set_params(pk.p, pk.q);

    auto enc = ure_encrypt(crypto, 5);
    ure_reencrypt(enc);
    auto dec = ure_decrypt(crypto, enc);

    std::cout << dec.ToString(10) << std::endl;

    return 0;*/


//#ifdef MEASURE_RUNTIME
    ahead::timing_main();
#if false

    auto m_psp = std::make_shared<psp>();
    auto adx = std::make_shared<ad_exchange>(m_psp);

    auto key_share = m_psp->generate_key_shares();
    std::shared_ptr<ahead::ThresholdPaillier> dsp_crypto = std::make_shared<ahead::ThresholdPaillier>(m_psp->get_public_key(), key_share);

    user_factory users;

    // Create DSP groups
    auto shamir = runtime::get().shamir;
    unsigned campaign_id = 1;
    for (size_t i = 0; i < runtime::get().n_dsp_groups; i++)
    {
        auto group = std::make_shared<dsp_group>(shamir, dsp_crypto, m_psp, adx, i);
        group->make_parties<dsp>();
        for (auto& party : group->parties)
        {
            auto d = std::dynamic_pointer_cast<dsp>(party);
            d->set_group(group);
            for (size_t i = 0; i < runtime::get().n_campaigns_per_dsp; i++)
            {
                d->add_campaign(campaign(campaign_id, campaign_id, 1, 1, 0.1));
                campaign_id++;
            }
        }
        adx->add_dsp_group(group);
    }

    // Let's request an ad!
    std::cout << "Opening data reader" << std::endl;
    ahead::DataReader reader(runtime::get().training_data, std::unique_ptr<ahead::AvazuDataParser>(new ahead::AvazuDataParser()));


    unsigned lines = 0;
    ahead::DataLine data;
    while (lines < runtime::get().training_samples && reader.get_line(data))
    {
        lines++;
        user u = users.get_user(data);

        for (auto elem : adx->dsp_groups)
        {
            auto shares = u.share();
            adx->share_profile(u.info, shares.first, *(elem.second));
            m_psp->share_profile(u.info, shares.second, *(elem.second));
        }

        bid_response result = adx->request_ad(u.info);

        //std::cout << "Bid id: " << result.bid_id << std::endl;
        BigInteger dec_ad = u.info.crypto_provider->DecryptInteger(result.enc_ad);
        BigInteger group_id = dec_ad & 0xFFFF;
        dec_ad >>= 16;
        //std::cout << "Group:  " << group_id.ToString(10) << std::endl;
        //std::cout << "Ad:     " << dec_ad.ToString(10) << std::endl;

        auto dsp_group = adx->dsp_groups.at(group_id.ToUnsignedLong());
        dsp_group->report_response(result.bid_id, u.info, data.label, result.enc_k);

        if (lines % 100 == 0)
        {
            for (auto& losses : m_psp->logloss)
            {
                unsigned k = losses.first;
                double loss = losses.second;
                unsigned count = m_psp->count[k];

                std::cout << "Logloss of " << k << " after " << count << " wins: " << loss / count << std::endl;
            }
        }
    }

#endif

    return 0;
}
