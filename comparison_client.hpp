#ifndef BADASS_COMPARISON_CLIENT_HPP
#define BADASS_COMPARISON_CLIENT_HPP

#include "shamir.hpp"

class comparison_group : public shamir::group
{
public:
    using group::group;

    void max(size_t n_values);
    shamir::value_t get_result(size_t n);
};

class comparison_party : public shamir::party
{
    friend class comparison_group;

public:
    using party::party;

    void setup_comparison();
    void push_comparison_list(shamir::share_vector&& list);
    void comparison_list_from_temp_stack();

    shamir::share_vector& comparison_result();
    void push_result(size_t const& n);

private:
    bool can_compare() const;
    void prepare_max();
    void set_new_max();
    void move_result();

    shamir::share_vector max_lists;
    std::vector<shamir::share_vector> comparison_lists;
};

#endif
