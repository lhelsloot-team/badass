#include "comparison_client.hpp"

void comparison_group::max(size_t n_values)
{
    auto party = std::dynamic_pointer_cast<comparison_party>(this->parties.front());

    this->call_derived<comparison_party>(&comparison_party::setup_comparison);
    while (party->can_compare())
    {
        this->call_derived<comparison_party>(&comparison_party::prepare_max);
        this->greater_than_equal();
        this->call_derived<comparison_party>(&comparison_party::set_new_max);

        // Reduce degree of new max values
        for (size_t i = 0; i < n_values; i++)
        {
            this->reduce_degree();
            this->call_derived<comparison_party>(&comparison_party::move_result);
        }
    }
}

shamir::value_t comparison_group::get_result(size_t n)
{
    this->call_derived<comparison_party>(&comparison_party::push_result, n);
    return this->combine();
}

void comparison_party::setup_comparison()
{
    this->max_lists = this->comparison_lists.back();
    this->comparison_lists.pop_back();
}

void comparison_party::push_comparison_list(shamir::share_vector&& list)
{
    this->comparison_lists.push_back(std::move(list));
}

void comparison_party::comparison_list_from_temp_stack()
{
    this->comparison_lists.emplace_back(this->temp_stack.begin(), this->temp_stack.end());
    this->temp_stack.clear();
    //this->comparison_lists.push_back(this->temp_stack);
}

shamir::share_vector& comparison_party::comparison_result()
{
    return this->max_lists;
}

void comparison_party::push_result(size_t const& n)
{
    this->push(this->max_lists.at(n));
}

bool comparison_party::can_compare() const
{
    return !this->comparison_lists.empty();
}

void comparison_party::prepare_max()
{
    auto& values = this->comparison_lists.front();

    // Perform comparison on first elements of values and max_lists
    this->push(values.front());
    this->push(this->max_lists.front());
}

void comparison_party::set_new_max()
{
    auto& values = this->comparison_lists.back();
    shamir::share_t rho = this->pop_top();

    // Perform computation in reverse order, as move_result pushes to back.
    for (int i = values.size() - 1; i >= 0; i--)
    {
        // Multiply rho with v_i
        this->push(rho);
        this->push(values[i]);
        this->multiply();

        // Multiply 1 - rho with v_max
        this->push(rho);
        this->multiply(-1);
        this->add(1);
        this->push(this->max_lists[i]);
        this->multiply();

        // Add the two together. Leave result on stack for share reduction.
        this->add();
    }

    this->max_lists.clear();
    this->comparison_lists.pop_back();
}

void comparison_party::move_result()
{
    this->max_lists.push_back(this->pop_top());
}
