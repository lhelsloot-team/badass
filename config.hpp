#ifndef BADASS_CONFIG_HPP
#define BADASS_CONFIG_HPP

#include "secomlib.hpp"
#include "shamir.hpp"

template<typename T>
class singleton
{
public:
    static T& get()
    {
        static T instance;
        return instance;
    }
};

class runtime : public singleton<runtime>
{
    friend class singleton<runtime>;

public:
    size_t const shamir_p;
    shamir::index_t const shamir_t;
    shamir::index_t const shamir_n;
    size_t const shamir_l;
    size_t const shamir_k;

    size_t const weight_bits;
    size_t const weight_integer_bits;
    size_t const fp_truncate_factor;
    size_t const fp_mult_factor;
    size_t const fp_space;
    size_t const fp_signed_boundary;
    double const fp_min_value;

    size_t const profile_bits;
    size_t const profile_d;
    size_t const n_dsp_groups;
    size_t const n_campaigns_per_dsp;
    size_t const dsp_update_threshold;

    std::string const training_data;
    size_t const training_samples;
    size_t const testing_samples;

    std::shared_ptr<shamir::shamir> shamir;

protected:
    runtime();
};

#endif
