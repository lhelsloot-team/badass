
#include "elgamal.hpp"


/// Default constructor
PlainElGamal::PlainElGamal ()
    : ElGamal() {}

/// Creates an instance of the class for homomorphic operations and ecryption
PlainElGamal::PlainElGamal (const ElGamalPublicKey &publicKey)
    : ElGamal(publicKey) {}

/// Creates an instance of the class for homomorphic operations, ecryption and decryption
PlainElGamal::PlainElGamal (const ElGamalPublicKey &publicKey, const ElGamalPrivateKey &privateKey)
    : ElGamal(publicKey, privateKey) {}

std::shared_ptr<PlainElGamal> PlainElGamal::ConstructFromConfig()
{
    std::string privatekey = Config::GetInstance().GetParameter("Core.ElGamal.privatekey", std::string());

    if (privatekey.empty())
    {
        auto crypto = std::make_shared<PlainElGamal>();

        std::cout << "Generating ElGamal keys..." << std::endl;

        crypto->GenerateKeys();

        auto& pk = crypto->GetPublicKey();
        auto& sk = crypto->GetPrivateKey();

        std::cout << "<privatekey>" << sk.s.ToString(16) << "</privatekey>" << std::endl;
        std::cout << "<publickey><p>" << pk.p.ToString(16) << "</p><q>" << pk.q.ToString(16) << "</q><gq>" << pk.gq.ToString(16) << "</gq><h>" << pk.h.ToString(16) << "</h></publickey>" << std::endl;

        return crypto;
    }

    else
    {
        std::cout << "Loading ElGamal keys..." << std::endl;

        ElGamalPublicKey pk;

        pk.p = BigInteger(Config::GetInstance().GetParameter<std::string>("Core.ElGamal.publickey.p"), 16);
        pk.q = BigInteger(Config::GetInstance().GetParameter<std::string>("Core.ElGamal.publickey.q"), 16);
        pk.gq = BigInteger(Config::GetInstance().GetParameter<std::string>("Core.ElGamal.publickey.gq"), 16);
        pk.h = BigInteger(Config::GetInstance().GetParameter<std::string>("Core.ElGamal.publickey.h"), 16);

        ElGamalPrivateKey sk { BigInteger(privatekey, 16) };

        return std::make_shared<PlainElGamal>(pk, sk);
    }
}

/**
 C *ontains the ElGamal encryption algorithm.

 @param plaintext the plaintext integer
 @return Encrypted ciphertext
 */
PlainElGamal::Ciphertext PlainElGamal::EncryptIntegerNonrandom (const BigInteger &plaintext) const {
    ElGamal::Ciphertext output(this->encryptionModulus);

    /// If @f$ plaintext < 0 @f$, we remap it to the second half of the message space

    /// Set @f$ x = 1 @f$ and randomize it later (replace it with @f$ g_q^r @f$)
    output.data.x = 1;
    if (plaintext < 0) {
        output.data.y = this->GetMessageSpaceUpperBound() + plaintext;
    }
    else {
        output.data.y = plaintext;
    }

    return output;
}

/**
 I *f @f$ plaintext \in [0, 2^t) \Rightarrow plaintext \geq 0 @f$ otherwise if @f$ plaintext \in [q - 2^t, q) \Rightarrow plaintext = plaintext - messageSpaceUpperBound  @f$

 @param ciphertext the ciphertext integer
 @return Deciphered plaintext
 @throws std::runtime_error the ciphertext can not be decrypted
 @throws std::runtime_error operation requires the private key
 */
BigInteger PlainElGamal::DecryptInteger (const ElGamal::Ciphertext &ciphertext) const {
    if (!this->hasPrivateKey) {
        throw std::runtime_error("This operation requires the private key.");
    }

    /*
    if (!this->precomputeDecryptionMap) {
        throw std::runtime_error("This operation requires the decryption map.");
    }
    */

    /// Compute @f$ c.y * c.x^{-s} \pmod p @f$
    BigInteger cyCxPowMinusSModP = (ciphertext.data.y * ciphertext.data.x.GetPowModN(-this->privateKey.s, this->publicKey.p)) % this->publicKey.p;

    BigInteger output = cyCxPowMinusSModP;

    /*
    /// Shortcut: if @f$ c.y * c.x^{-s} \pmod p = 1 @f$, then @f$ c = [0] @f$
    if (cyCxPowMinusSModP == 1) {
        return 0;
    }

    /// @f$ m \in \mathbb{Z}_q \ (2^t, \lfloor q / 2 \rfloor + 2^t) @f$ is uniquely determined by @f$ g_q^m \pmod p @f$.
    /// Since we cannot determine m directly, we precompute all @f$ g_q^m \pmod p @f$ values, we store them in an std::map and we try to find m that matches @f$ c.y * c.x^{-s} \pmod p @f$
    BigInteger output;

    //get an iterator to the required element
    DecryptionMap::const_iterator iterator = this->decryptionMap.find(cyCxPowMinusSModP);

    //make sure the key exists
    if (this->decryptionMap.end() != iterator) {
        output = BigInteger(iterator->second);
    }
    else {
        throw std::runtime_error("Can't decrypt ciphertext.");
    }
    */

    if (output > this->positiveNegativeBoundary) {
        output -= this->GetMessageSpaceUpperBound();
    }

    return output;
}
