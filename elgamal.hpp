#ifndef BADASS_ELGAMAL_HPP
#define BADASS_ELGAMAL_HPP

#include "secomlib.hpp"

/**
 * @brief Implementation of a two-party Paillier cryptosystem.
 */
class PlainElGamal : public ElGamal
{
public:
    /// Default constructor
    PlainElGamal ();

    /// Creates an instance of the class for homomorphic operations and ecryption
    PlainElGamal (const ElGamalPublicKey &publicKey);

    /// Creates an instance of the class for homomorphic operations, ecryption and decryption
    PlainElGamal (const ElGamalPublicKey &publicKey, const ElGamalPrivateKey &privateKey);

    static std::shared_ptr<PlainElGamal> ConstructFromConfig();

    /// Generate a key pair and split the private key into two shares.
    //bool GenerateKeys() override;

    /// Encrypt number without randomization
    virtual Ciphertext EncryptIntegerNonrandom (const BigInteger &plaintext) const override;

    BigInteger DecryptInteger(Ciphertext const& ciphertext) const override;
};

#endif
