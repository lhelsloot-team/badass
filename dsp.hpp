#ifndef BADASS_DSP_HPP
#define BADASS_DSP_HPP

#include <unordered_map>
#include <memory>

#include "shamir.hpp"
#include "user.hpp"
#include "comparison_client.hpp"

class campaign_share;
class dsp_group;
class psp;
class ad_exchange;

class campaign
{
public:
    typedef uint16_t id_t;

    campaign (id_t campaign_id, unsigned ad_arg, unsigned c1_arg, unsigned c2_arg, double eta_arg);

    std::vector<campaign_share> share() const;
    user::ciphertext_t get_ad(user_info const& user) const;

    id_t id;

private:
    std::vector<float> w;

    unsigned ad;

    unsigned c1;
    unsigned c2;

    unsigned eta;
};

class campaign_share
{
public:
    campaign_share (unsigned campaign_id, shamir::share_vector const& w_arg, shamir::share_t const& c1_arg, shamir::share_t const& c2_arg, shamir::share_t const& eta_arg);

    void bidding_function(shamir::party& party, shamir::share_t const& yhat_share) const;
    void update_w(shamir::share_vector g_shares);

    campaign::id_t id;
    shamir::share_vector w;
    shamir::share_t eta;

private:
    shamir::share_t c1;
    shamir::share_t c2;
};

class dsp : public comparison_party
{
public:
    struct bid_info
    {
        shamir::share_t b;
        shamir::share_t yhat;
        shamir::share_t eta;
        shamir::share_t k;
    };

    typedef std::vector<shamir::share_t> share_deque_t;
    typedef std::vector<share_deque_t> shares_vector_t;
    typedef std::array<std::vector<shamir::share_vector>, 5> update_shares_t;
    typedef std::array<update_shares_t, 5> update_shares_array_t;
    typedef std::array<std::vector<shamir::share_t>, 5> inverted_shares_t;
    typedef std::array<inverted_shares_t, 5> inverted_shares_array_t;

    using comparison_party::comparison_party;

    void set_group(std::shared_ptr<dsp_group> group_arg) { this->group = group_arg; }

    void add_campaign(campaign const& campaign);
    void add_campaign_share(campaign_share const& share);

    void combine_profile(user_profile_share&& x);

    void calculate_bid_share(user_info const& user, size_t const& offset);
    void run_bidding_function(size_t const& offset);
    void submit_bid(unsigned const& bid_id, user_info const& user, size_t const& offset, unsigned const& group_id);
    void store_max(unsigned const& bid_id);
    void prepare_comparison();

    void lookup_bid_info(unsigned const& bid_id);
    //void calculate_delta(unsigned const& bid_id);
    void calculate_update (user_info const& user);
    bool should_update_model() const;
    void prepare_model_update();
    void combine_k(inverted_shares_t& k_shares);
    void update_model(update_shares_t& gradient_shares, inverted_shares_t& b_shares, std::vector<shamir::value_t> const& k_vector);

    template<typename T>
    void rotate_shares(T&& vec)
    {
        size_t n_shares = vec.size();
        for (size_t i = 0; i < n_shares; i++)
        {
            size_t r = std::static_pointer_cast<dsp>(std::static_pointer_cast<shamir::group>(this->group)->parties[i])->rotation_r;
            std::rotate(vec[i].rbegin(), vec[i].rbegin() + r, vec[i].rend());
        }
    }

    void clear_data();



//private:

    shamir::share_vector& find_campaign(campaign::id_t k);

    /// Pointer to the DSP group within which computations for this DSP are
    /// performed.
    std::shared_ptr<dsp_group> group;

    std::unordered_map<campaign::id_t, campaign> own_campaigns;
    std::unordered_map<campaign::id_t, size_t> campaign_map;
    std::vector<campaign_share> campaign_shares;

    std::unordered_map<user::id_t, user_profile_share> user_shares;
    std::unordered_map<unsigned, bid_info> previous_bids;

    shares_vector_t stored_gradients;
    share_deque_t stored_b;
    share_deque_t stored_ks;
    unsigned rotation_r;
};

class dsp_group : public comparison_group
{
    friend class dsp;

public:
    dsp_group(std::shared_ptr<shamir::shamir> shamir, std::shared_ptr<psp> psp_arg, std::shared_ptr<ad_exchange> adx_arg, unsigned id_arg);

    void request_bids (unsigned int bid_id, user_info const& user);
    void submit_bids(dsp_group& comparison_group);

    void report_response(unsigned bid_id, user_info const& user, int y, unsigned long k);
    double get_logloss(unsigned bid_id, int y);

    void update_model(dsp::update_shares_array_t& gradient_shares, dsp::inverted_shares_array_t& k_shares, dsp::inverted_shares_array_t& b_shares);

    size_t n_campaigns;
    unsigned id;

protected:
    template<typename... Args>
    void call(void (dsp::*fun)(typename std::decay<Args>::type const&...), Args&&... args)
    {
        for (auto& party : this->parties)
        {
            (*std::dynamic_pointer_cast<dsp>(party).*fun)(std::forward<Args>(args)...);
        }
    }

    //std::shared_ptr<ahead::ThresholdPaillier> crypto_provider;
    std::shared_ptr<psp> m_psp;
    std::shared_ptr<ad_exchange> adx;
};

#endif
