#ifndef BADASS_RANDOM_POOL
#define BADASS_RANDOM_POOL

#include <random>

class random_generator
{
public:
    typedef std::random_device::result_type result_type;

    static size_t const random_pool_size = 100;
    static size_t const default_pool_size = 400;

    random_generator();
    void fill_pool();

    template<typename T>
    void fill_default_pool(T&& dis)
    {
        for (size_t i = 0; i < default_pool_size; i++)
        {
            default_pool[i] = dis(dev);
        }
    }

    result_type min() { return dev.min(); }
    result_type max() { return dev.max(); }
    result_type entropy() { return dev.entropy(); }

    result_type operator()();

    result_type get_default();

private:
    std::random_device dev;
    result_type random_pool[random_pool_size];
    result_type default_pool[default_pool_size];
};

class random_pool
{
    random_generator gen;

public:
    template<typename T>
    void fill_default_pool(T&& dis)
    {
        gen.fill_default_pool(std::forward<T>(dis));
    }

    random_generator::result_type get() { return gen.get_default(); }
    random_generator::result_type get(random_generator::result_type max);
};

#endif
