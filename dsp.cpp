#include "config.hpp"

#include "dsp.hpp"
#include "psp.hpp"
#include "ad_exchange.hpp"
#include "timing.hpp"

campaign::campaign (id_t campaign_id, unsigned ad_arg, unsigned c1_arg, unsigned c2_arg, double eta_arg)
    : id(campaign_id)
    , w(runtime::get().profile_d)
    , ad(ad_arg)
    , c1(c1_arg)
    , c2(c2_arg)
    , eta(fp::to_fp(eta_arg)) {}

std::vector<campaign_share> campaign::share() const
{
    auto shamir = runtime::get().shamir;

    std::vector<shamir::share_vector> w_shares(shamir->n);
    for (size_t i = 0; i < runtime::get().profile_d; i++)
    {
        shamir::shares_t shares = shamir->split(fp::to_fp(this->w[i]));
        for (size_t j = 0; j < shares.size(); j++)
        {
            w_shares[j].push_back(shares[j]);
        }
    }

    shamir::shares_t c1_shares = shamir->split(this->c1);
    shamir::shares_t c2_shares = shamir->split(this->c2);
    shamir::shares_t eta_shares = shamir->split(this->eta);

    std::vector<campaign_share> shares;
    for (size_t j = 0; j < shamir->n; j++)
    {
        shares.emplace_back(campaign_share(this->id, w_shares[j], c1_shares[j], c2_shares[j], eta_shares[j]));
    }

    return shares;
}

user::ciphertext_t campaign::get_ad(user_info const& user) const
{
    // Treat ad | Gamma as a single large integer
    //BigInteger a(this->ad);
    //a <<= 8;
    //a |= group_id;
    return ure_encrypt(*user.crypto_provider, this->ad);
}

campaign_share::campaign_share (unsigned campaign_id, shamir::share_vector const& w_arg, shamir::share_t const& c1_arg, shamir::share_t const& c2_arg, shamir::share_t const& eta_arg)
    : id(campaign_id)
    , w(w_arg)
    , c1(c1_arg)
    , c2(c2_arg)
    , eta(eta_arg) {}

void campaign_share::bidding_function(shamir::party& party, shamir::share_t const& yhat_share) const
{
    shamir::share_t b = yhat_share * this->c1;
    b += this->c2;
    party.push(b);
}

void dsp::add_campaign(campaign const& c)
{
    this->own_campaigns.insert(std::make_pair(c.id, c));

    std::vector<campaign_share> shares = c.share();
    auto it = shares.begin();
    for (auto& party : this->group->parties)
    {
        auto d = std::dynamic_pointer_cast<dsp>(party);
        d->add_campaign_share(*it);
        it++;
    }
    this->group->n_campaigns++;
}

void dsp::add_campaign_share(campaign_share const& share)
{
    this->campaign_map[share.id] = this->campaign_shares.size();
    this->campaign_shares.push_back(share);
}

static Timer<timer_id::update_profile_dsp, true> upd_timer("update_profile_dsp");
void dsp::combine_profile(user_profile_share&& profile)
{
    upd_timer.start();
    if (this->user_shares.count(profile.info.id))
    {
        auto& existing_profile = this->user_shares.at(profile.info.id);

        if (existing_profile.complete)
        {
            existing_profile = std::move(profile);
        }
        else
        {
            existing_profile.x += profile.x;
            existing_profile.complete = true;
        }
    }
    else
    {
        this->user_shares.insert(std::make_pair(profile.info.id, std::move(profile)));
    }
    upd_timer.stop();
}

void dsp::calculate_bid_share(user_info const& user, size_t const& offset)
{
    user_profile_share const& profile = this->user_shares.at(user.id);
    campaign_share const& campaign = this->campaign_shares.at(offset);

    // Calculate s = sum(x_i*w_i)
    // Stack bottom is the sum s, starting with 0
    // Then repeatedly push multiplication arguments and add the result
    //this->push(shamir::share_t(this->x, 0, this->s->p));

    size_t profile_d = runtime::get().profile_d;
    //shamir::share_t sum(0);
    uint64_t sum = 0;

    for (size_t i = 0; i < profile_d; i++)
    {
        //shamir::share_t prod = profile.x[i];
        //prod *= campaign.w[i];
        //sum += prod;

        uint64_t prod = profile.x[i].y;
        prod *= campaign.w[i].y;
        sum = (sum + prod) % shamir::p;

        //this->push(profile.x[i]);
        //this->push(campaign.w[i]);
        //this->multiply();
        //this->add();
    }

    this->push(sum);
    // Stack top is now the inner product of x and w
}

void dsp::run_bidding_function(size_t const& offset)
{
    // STACK: yhat
    campaign_share const& campaign = this->campaign_shares.at(offset);
    this->push(campaign.eta);
    campaign.bidding_function(*this, this->at(1));
    // STACK: b η yhat
}

void dsp::submit_bid(unsigned const& bid_id, user_info const& user, size_t const& offset, unsigned const& group_id)
{
    // STACK: b η yhat
    campaign_share const& share = this->campaign_shares.at(offset);
    campaign::id_t id = share.id;

    if (this->own_campaigns.count(id))
    {
        campaign const& campaign = this->own_campaigns.at(id);

        //shamir::value_t r_a = shamir::to_value(RandomProvider::GetInstance().GetRandomInteger(BigInteger(this->s->p)));
        shamir::value_t r_a = this->s->randomizer();
        this->group->split(r_a);
        this->group->split(id);

        auto psp = this->group->m_psp;

        bid_response bid;
        bid.bid_id = bid_id;
#ifndef NO_CRYPTO
        bid.enc_ad = campaign.get_ad(user);
#endif
        bid.group_id = group_id;
        bid.k = id;

        this->group->adx->submit_bid(shamir::to_number(r_a), bid);
    }

    // STACK: k r_a b η yhat
}

void dsp::prepare_comparison()
{
    // STACK: k r_a b η yhat
    shamir::share_t k = this->pop_top();
    shamir::share_t r_a  = this->pop_top();
    shamir::share_t b = this->pop_top();
    shamir::share_t eta = this->pop_top();

    // Push (b, r_a, y_hat, η, k) shares for comparison
    shamir::share_vector c_list;
    c_list.reserve(5);

    c_list.push_back(b);
    c_list.push_back(r_a);
    c_list.push_back(this->pop_top());
    c_list.push_back(eta);
    c_list.push_back(k);

    this->push_comparison_list(std::move(c_list));
}

void dsp::store_max(unsigned const& bid_id)
{
    // Store id_bid -> (y_hat, b, η)
    // Comparison result: (b, r_a, y_hat, η)
    shamir::share_vector& result = this->comparison_result();

    this->previous_bids.erase(bid_id - 10);
    this->previous_bids.insert(std::make_pair(bid_id, bid_info { result[2], result[0], result[3], result[4] } ));

    // Push b and r_a for sharing with comparison group
    this->push(result[0]);
    this->push(result[1]);
}

void dsp::lookup_bid_info(unsigned const& bid_id)
{
    auto& bid_info = this->previous_bids.at(bid_id);
    this->push(bid_info.b);
    this->push(bid_info.k);
    this->push(bid_info.yhat);
    this->push(bid_info.eta);
    this->dup();
}

void dsp::calculate_update (user_info const& user)
{
    // STACK: η(yhat - y) k b
    user_profile_share& profile = this->user_shares.at(user.id);
    shamir::share_t& delta = this->top();
    size_t profile_d = runtime::get().profile_d;

    // Multiply without degree reduction, as degree is reduced automatically
    // when sharing with PSP
    //this->clear_temp_stack();
    //for (int i = runtime::get().profile_d - 1; i >= 0; i--)
    //{
        //this->dup();
        //this->push(profile.x.at(i));
        //this->multiply();
        //this->move_to_temp_stack();
    //}

    this->stored_gradients.emplace_back();
    auto& g = this->stored_gradients.back();
    g.reserve(profile_d);
    for (size_t i = 0; i < profile_d; i++)
    {
        g[i] = delta * profile.x[i];
    }

    // Don't need delta anymore
    this->pop();

    // Push b and k
    this->stored_ks.push_back(this->pop_top());
    this->stored_b.push_back(this->pop_top());

    //this->stack = this->temp_stack;
    //this->push(delta);
    //this->push(bid_info.first);

    //this->stored_gradients.push_back(std::move(this->temp_stack));
    //this->stored_gradients.emplace_back(this->temp_stack.begin(), this->temp_stack.end());
    //this->temp_stack.clear();
    //this->stored_k.push_back(this->group->m_psp->public_crypto->RandomizeCiphertext(enc_k));
}

bool dsp::should_update_model() const
{
    return this->stored_gradients.size() >= runtime::get().dsp_update_threshold;
}

void dsp::prepare_model_update()
{
    // Rotate stored gradients and k with a random r
    //this->rotation_r = RandomProvider::GetInstance().GetRandomInteger(BigInteger(runtime::get().dsp_update_threshold)).ToUnsignedLong();
    //std::uniform_int_distribution<size_t> dis(0, runtime::get().dsp_update_threshold - 1);
    //this->rotation_r = dis(this->s->random_dev);
    this->rotation_r = this->s->random.get(runtime::get().dsp_update_threshold - 1);

    std::rotate(this->stored_gradients.begin(), this->stored_gradients.begin() + this->rotation_r, this->stored_gradients.end());
    std::rotate(this->stored_ks.begin(), this->stored_ks.begin() + this->rotation_r, this->stored_ks.end());
    std::rotate(this->stored_b.begin(), this->stored_b.begin() + this->rotation_r, this->stored_b.end());
    //std::rotate(this->stored_k.begin(), this->stored_k.begin() + this->rotation_r, this->stored_k.end());

    this->group->m_psp->submit_mix_shares(std::move(this->stored_gradients), std::move(this->stored_ks), std::move(this->stored_b));
}

void dsp::combine_k(inverted_shares_t& k_shares)
{
    size_t n_shares = k_shares.size();
    size_t n_gradients = runtime::get().dsp_update_threshold;
    this->rotate_shares(k_shares);

    shamir::shares_t rotated_shares;
    //rotated_shares.reserve(n_shares);

    for (int i = n_gradients - 1; i >= 0; i--)
    //for (int i = 0; i < n_gradients; i++)
    {
        for (size_t source_dsp = 0; source_dsp < n_shares; source_dsp++)
        {
            rotated_shares[source_dsp] = k_shares[source_dsp][i];
        }
        shamir::share_t my_share = s->reduce_degree_combine(rotated_shares);
        //rotated_shares.clear();
        this->push(my_share);
    }
}

shamir::share_vector& dsp::find_campaign(campaign::id_t k)
{
    size_t index = this->campaign_map[k];
    return this->campaign_shares[index].w;
}

void dsp::update_model(update_shares_t& gradient_shares, inverted_shares_t& b_shares, std::vector<shamir::value_t> const& k_vector)
{
    size_t n_shares = group->parties.size();
    size_t n_gradients = runtime::get().dsp_update_threshold;
    size_t profile_d = runtime::get().profile_d;

    this->rotate_shares(gradient_shares);
    this->rotate_shares(b_shares);

    // Rotate received vectors
    /*for (size_t i = 0; i < n_shares; i++)
    {
        size_t r = std::dynamic_pointer_cast<dsp>(group->parties[i])->rotation_r;
        std::rotate(gradient_shares[i].rbegin(), gradient_shares[i].rbegin() + r, gradient_shares[i].rend());
        std::rotate(k_shares[i].rbegin(), k_shares[i].rbegin() + r, k_shares[i].rend());
        std::rotate(b_shares[i].rbegin(), b_shares[i].rbegin() + r, b_shares[i].rend());
    }

    std::rotate(enc_k.rbegin(), enc_k.rbegin() + this->rotation_r, enc_k.rend());
    std::rotate(dec_k.rbegin(), dec_k.rbegin() + this->rotation_r, dec_k.rend());*/


    // Process gradients
    //shamir::share_vector combined_gradient;
    //combined_gradient.reserve(profile_d);

    shamir::shares_t rotated_shares;
    //rotated_shares.reserve(n_shares);

    for (size_t gradient = 0; gradient < n_gradients; gradient++)
    {
        // Decrypt k
        //auto own_share = group->crypto_provider->ShareDecrypt(enc_k[gradient]);
        //auto k = this->group->crypto_provider->CombineShares(own_share, dec_k[gradient]);

        auto k = k_vector[gradient];
        shamir::share_vector& w = this->find_campaign(k);

        // Reconstruct shares of gradient

        for (size_t i = 0; i < profile_d; i++)
        {
            for (size_t source_dsp = 0; source_dsp < n_shares; source_dsp++)
            {
                rotated_shares[source_dsp] = gradient_shares[source_dsp][gradient][i];
            }
            shamir::share_t my_share = s->reduce_degree_combine(rotated_shares);
            w[i] -= my_share;
            //rotated_shares.clear();
            //combined_gradient.push_back(my_share);
        }

        // Now perform the actual model update
        // Find campaign share
        /*bool found = false;
        for (auto& share : this->campaign_shares)
        {
            if (share.id == k)
            {
                share.w -= combined_gradient;

                found = true;
                break;
            }
        }

        if (!found)
        {
            throw std::invalid_argument("Campaign to update is unknown to DSP");
        }

        combined_gradient.clear();*/
    }

    /*
    // Decrypt k
    std::vector<BigInteger> k;
    for (size_t i = 0; i < n_gradients; i++)
    {
        auto own_share = group->crypto_provider->ShareDecrypt(enc_k[i]);
        k.push_back(this->group->crypto_provider->CombineShares(own_share, dec_k[i]));
        std::cout << "Decrypted k: " << k.back().ToString(10) << std::endl;
    }

    // Reconstruct shares of secret
    std::vector<shamir::share_vector> combined_gradients(n_gradients);
    for (size_t gradient = 0; gradient < n_gradients; gradient++)
    {
        for (size_t i = 0; i < runtime::get().profile_d; i++)
        {
            shamir::shares_t rotated_shares;
            for (size_t source_dsp = 0; source_dsp < n_shares; source_dsp++)
            {
                rotated_shares.push_back(gradient_shares[source_dsp][gradient][i]);
            }
            shamir::share_t my_share = s->reduce_degree_combine(rotated_shares);
            combined_gradients[(gradient + this->rotation_r) % n_gradients].push_back(my_share);
            this->push(my_share);
        }
    }
    */
}

void dsp::clear_data()
{
    this->user_shares.clear();
    this->previous_bids.clear();
}

dsp_group::dsp_group(std::shared_ptr<shamir::shamir> shamir, std::shared_ptr<psp> psp_arg, std::shared_ptr<ad_exchange> adx_arg, unsigned id_arg)
    : comparison_group(shamir)
    , n_campaigns(0)
    , id(id_arg)
    , m_psp(psp_arg)
    , adx(adx_arg) {}

void dsp_group::request_bids(unsigned bid_id, user_info const& user)
{
    //size_t r = RandomProvider::GetInstance().GetRandomInteger(BigInteger(this->n_campaigns)).ToUnsignedLong();
    //std::uniform_int_distribution<size_t> dis(0, this->n_campaigns-1);
    //size_t r = dis(this->s->random_dev);
    size_t r = this->s->random.get(this->n_campaigns-1);

    for (size_t i = 0; i < this->n_campaigns; i++)
    {
        size_t offset = (i + r) % this->n_campaigns;
        this->call(&dsp::calculate_bid_share, user, offset);
        this->m_psp->calculate_sigma(*this);
        this->call(&dsp::run_bidding_function, offset);
        this->reduce_degree();
        this->call(&dsp::submit_bid, bid_id, user, offset, this->id);
        this->call(&dsp::prepare_comparison);
    }

    // Now perform the comparison
    this->max(5);

    // Store max values and prepare sharing with comparison group
    this->call(&dsp::store_max, bid_id);
}

void dsp_group::submit_bids(dsp_group& comparison_group)
{
    comparison_group.call(&dsp::clear_temp_stack);
    this->reshare(comparison_group);
    comparison_group.call(&dsp::move_to_temp_stack);
    this->reshare(comparison_group);
    comparison_group.call(&dsp::move_to_temp_stack);
    comparison_group.call(&dsp::comparison_list_from_temp_stack);
}

static Timer<timer_id::model_update_prepare, true> model_timer("update_model_prepare");
void dsp_group::report_response(unsigned bid_id, user_info const& user, int y, unsigned long k)
{
    model_timer.start();
    this->call(&dsp::lookup_bid_info, bid_id);

    // STACK: η η yhat k b

    this->split(fp::to_fp(y));
    this->multiply();
    this->truncate(runtime::get().fp_truncate_factor);
    this->multiply(-1);

    // STACK:
    // -(η y) η yhat k b

    this->call(&dsp::swap, 0, 2);

    // STACK:
    // yhat η -(η y) k b

#ifdef PRINT_LOGLOSS
    this->call(&dsp::dup);
    double yhat = fp::to_double(this->combine());

    yhat = std::max(std::min(yhat, 1. - 10e-12), 10e-12);
    if (y)
    {
        m_psp->report_logloss(std::log(yhat), k);
    }
    else
    {
        m_psp->report_logloss(std::log(1. - yhat), k);
    }
#endif

    this->multiply();
    this->truncate(runtime::get().fp_truncate_factor);

    // STACK:
    // (yhat η) -(y η) k b

    this->add();


    // STACK:
    // η(yhat - y) k b

    // Multiply with eta, and truncate since we're doing fixed-point multiplication
    //this->multiply(fp::to_fp(0.1));
    //this->truncate(runtime::get().fp_truncate_factor);

    // Push shares of -y * eta and add to get (yhat - y) * eta
    //this->split(fp::to_fp(-y * 0.1));
    //this->add();

    //this->split(fp::to_fp(-y * 0.1));
    //this->call(&dsp::calculate_delta, bid_id);

    //this->truncate(runtime::get().fp_truncate_factor);

    //this->call(&shamir::party::dup);
    //double delta = fp::to_double(this->combine());
    //std::cout << "y: " << y << ", σ: " << yhat << ", δ: " << delta << std::endl;

    this->call(&dsp::calculate_update, user);

    auto party = std::dynamic_pointer_cast<dsp>(this->parties.front());
    if (party->should_update_model())
    {
        this->call(&dsp::prepare_model_update);
        model_timer.stop();
        this->m_psp->mix_shares(*this);
    }
    else
    {
        model_timer.stop();
    }
}

double dsp_group::get_logloss(unsigned bid_id, int y)
{
    this->call(&dsp::lookup_bid_info, bid_id);

    // Pop two etas
    this->call(&dsp::pop);
    this->call(&dsp::pop);

    double yhat = fp::to_double(this->combine());

    yhat = std::max(std::min(yhat, 1. - 10e-12), 10e-12);
    if (y)
    {
        return std::log(yhat);
    }
    else
    {
        return std::log(1. - yhat);
    }
}


static Timer<timer_id::model_update_update, true> model_upd_timer("update_model_update");
void dsp_group::update_model(dsp::update_shares_array_t& gradient_shares, dsp::inverted_shares_array_t& k_shares, dsp::inverted_shares_array_t& b_shares)
{
    model_upd_timer.start();
    size_t n_shares = this->parties.size();
    size_t n_gradients = runtime::get().dsp_update_threshold;

    for (size_t i = 0; i < k_shares.size(); i++)
    {
        std::static_pointer_cast<dsp>(this->parties.at(i))->combine_k(k_shares[i]);
    }

    std::vector<shamir::value_t> k_vector;
    k_vector.reserve(n_gradients);
    for (size_t i = 0; i < n_gradients; i++)
    {
        shamir::value_t v = this->combine();
        k_vector.push_back(v);
    }

    for (size_t i = 0; i < n_shares; i++)
    {
        auto d = std::static_pointer_cast<dsp>(this->parties.at(i));
        d->update_model(gradient_shares.at(i), b_shares.at(i), k_vector);
    }
    model_upd_timer.stop();
}
