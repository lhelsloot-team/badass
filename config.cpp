#include "config.hpp"
#include "secomlib.hpp"

runtime::runtime()
    : shamir_p(Config::GetInstance().GetParameter<uint64_t>("ShamirAds.shamir.p"))
    , shamir_t(Config::GetInstance().GetParameter<shamir::index_t>("ShamirAds.shamir.t"))
    , shamir_n(Config::GetInstance().GetParameter<shamir::index_t>("ShamirAds.shamir.n"))
    , shamir_l(Config::GetInstance().GetParameter<size_t>("ShamirAds.shamir.l"))
    , shamir_k(Config::GetInstance().GetParameter<size_t>("ShamirAds.shamir.k"))

    , weight_bits(Config::GetInstance().GetParameter<size_t>("ShamirAds.weight_bits", 16))
    , weight_integer_bits(Config::GetInstance().GetParameter<size_t>("ShamirAds.weight_integer_bits", 4))
    , fp_truncate_factor(weight_bits - weight_integer_bits - 1)
    , fp_mult_factor(static_cast<size_t>(1 << (fp_truncate_factor)))
    //, fp_space(1 << weight_bits)
    , fp_space(shamir_p)
    //, fp_signed_boundary(1 << (weight_bits - 1))
    , fp_signed_boundary(shamir_p / 2)
    , fp_min_value(1. / fp_mult_factor)

    , profile_bits(Config::GetInstance().GetParameter<size_t>("ShamirAds.profile_bits"))
    , profile_d(1 << profile_bits)
    , n_dsp_groups(Config::GetInstance().GetParameter<size_t>("ShamirAds.n_dsp_groups"))
    , n_campaigns_per_dsp(Config::GetInstance().GetParameter<size_t>("ShamirAds.campaigns_per_dsp"))
    , dsp_update_threshold (Config::GetInstance().GetParameter<size_t>("ShamirAds.dsp_update_threshold"))
    , training_data(Config::GetInstance().GetParameter<std::string>("ShamirAds.training_data"))
    , training_samples(Config::GetInstance().GetParameter<unsigned long>("ShamirAds.training_samples"))
    , testing_samples(Config::GetInstance().GetParameter<unsigned long>("ShamirAds.testing_samples"))
    , shamir(std::make_shared<shamir::shamir>(shamir_t, shamir_n, shamir_l, shamir_k))
{}
