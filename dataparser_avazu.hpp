#ifndef AHEAD_DATAPARSER_AVAZU_HPP
#define AHEAD_DATAPARSER_AVAZU_HPP

#include "datareader.hpp"
#include <vector>

namespace ahead
{

class AvazuDataParser : public DataParser
{
public:
    void initialize(std::istream & is) override;
    bool parse_line(std::istream & is, DataLine& data_line) override;

protected:
    std::vector<std::string> columns;
};

}

#endif
