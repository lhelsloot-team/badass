cmake_minimum_required(VERSION 2.6)
project(badass)

add_definitions(-DLIB_GMP)
#add_definitions(-DUSE_INCREMENTAL_UPDATES)
add_definitions(-DMEASURE_RUNTIME)
#add_definitions(-DPRINT_LOGLOSS)
#add_definitions(-DNO_CRYPTO)

include_directories(SeComLib)

file(GLOB ahead_SRC
    "*.hpp"
    "*.cpp"
)

add_executable(badass ${ahead_SRC})

#target_link_libraries(badass ${CMAKE_SOURCE_DIR}/SeComLib/_output/linux/libPrivateRecommendationsUtils.a)
target_link_libraries(badass ${CMAKE_SOURCE_DIR}/SeComLib/_output/linux/libCore.a)
target_link_libraries(badass ${CMAKE_SOURCE_DIR}/SeComLib/_output/linux/libUtils.a)
target_link_libraries(badass boost_timer)
target_link_libraries(badass gmp)

install(TARGETS badass RUNTIME DESTINATION bin)
