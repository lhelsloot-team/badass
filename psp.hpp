#ifndef BADASS_PSP_HPP
#define BADASS_PSP_HPP

#include "ad_exchange.hpp"
#include "dsp.hpp"
#include "config.hpp"
//#include "threshold_paillier.hpp"

class psp : public share_converter
{
public:
    psp();
    //ahead::ThresholdPaillier::PrivateKeyShare generate_key_shares();
    //SeComLib::Core::PaillierPublicKey const& get_public_key() const;

    void calculate_sigma(dsp_group& dsps);
    void submit_bid(shamir::index_t r_a, bid_response const& bid);
    bid_response get_ad(unsigned bid_id, dsp_group& comparison_group);

    void submit_mix_shares(dsp::shares_vector_t&& gradients, dsp::share_deque_t&& k, dsp::share_deque_t&& b);
    void mix_shares(dsp_group& group);

    void report_logloss(double loss, unsigned long k);
    void report_global_logloss(double loss);

    //std::shared_ptr<ahead::ThresholdPaillier> public_crypto;

    std::unordered_map<unsigned, double> logloss;
    std::unordered_map<unsigned, unsigned> count;

private:
    //std::shared_ptr<ahead::ThresholdPaillier> crypto_provider;
    std::unordered_map<unsigned, std::unordered_map<shamir::index_t, bid_response>> bids;

    std::vector<std::tuple<dsp::shares_vector_t, dsp::share_deque_t, dsp::share_deque_t>> shares_to_mix;

    dsp::update_shares_array_t split_gradients;
    dsp::inverted_shares_array_t split_k, split_b;
};

#endif
