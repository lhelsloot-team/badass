#ifndef BADASS_UNIVERSAL_REENCRYPTION_HPP
#define BADASS_UNIVERSAL_REENCRYPTION_HPP

#include "secomlib.hpp"

typedef std::pair<ElGamal::Ciphertext, ElGamal::Ciphertext> ur_ciphertext;

void ure_set_params(BigInteger const& p, BigInteger const& q);
void ure_reencrypt(ur_ciphertext& ciphertext);
ur_ciphertext ure_encrypt(ElGamal::CryptoProvider const& crypto, BigInteger const& plaintext);
BigInteger ure_decrypt(ElGamal::CryptoProvider const& crypto, ur_ciphertext const& ciphertext);

#endif
