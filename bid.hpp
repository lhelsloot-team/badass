#ifndef BADASS_BID_HPP
#define BADASS_BID_HPP

#include "user.hpp"

struct bid_response
{
    unsigned long bid_id;
    user::ciphertext_t enc_ad;
    unsigned long group_id;

    // Used for testing logloss
    unsigned long k;
};

#endif
