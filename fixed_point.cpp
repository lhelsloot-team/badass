
#include "fixed_point.hpp"
#include "config.hpp"

/*const size_t fp::w = runtime::get().weight_bits;
const size_t fp::p = runtime::get().weight_integer_bits;
const size_t fp::mult_factor = static_cast<size_t>(1 << (fp::w - fp::p - 1));
const long fp::fp_space = 1 << fp::w;
const long fp::signed_boundary = 1 << (fp::w - 1);
const double fp::min_value = 1. / fp::mult_factor;*/

unsigned fp::to_fp (double n)
{
    auto& rt = runtime::get();

    int r = static_cast<int> (std::round (n * rt.fp_mult_factor));

    if (r < 0)
    {
        r += rt.fp_space;
    }

    else if (r >= rt.fp_space)
    {
        throw std::runtime_error ("to_fp argument out of bounds");
    }

    return static_cast<unsigned> (r);
}

double fp::to_double (unsigned long n)
{
    auto& rt = runtime::get();

    n &= static_cast<unsigned>(rt.fp_space - 1);

    signed r = static_cast<signed> (n);

    if (r >= rt.fp_signed_boundary)
    {
        r -= rt.fp_space;
    }

    return static_cast<double> (r) / rt.fp_mult_factor;
}

double fp::to_double (BigInteger const& n)
{
    auto& rt = runtime::get();

    BigInteger i = n & static_cast<long>(rt.fp_space - 1);
    return to_double(i.ToUnsignedLong());
}

BigInteger fp::to_signed_biginteger (double n)
{
    auto& rt = runtime::get();

    return BigInteger (static_cast<long> (std::round (n * rt.fp_mult_factor)));
}
