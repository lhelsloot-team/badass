#include "universal_reencryption.hpp"

static BigInteger ure_p;
static BigInteger ure_q;

void ure_set_params(BigInteger const& p, BigInteger const& q)
{
    ure_p = p;
    ure_q = q;
}

void ure_reencrypt(ur_ciphertext& ciphertext)
{
    auto& c1 = ciphertext.first;
    auto& c2 = ciphertext.second;

    BigInteger k0p = RandomProvider::GetInstance().GetRandomInteger(ure_q);
    BigInteger k1p = RandomProvider::GetInstance().GetRandomInteger(ure_q);

    c1.data.x = (c1.data.x * c2.data.x.GetPowModN(k0p, ure_p)) % ure_p;
    c1.data.y = (c1.data.y * c2.data.y.GetPowModN(k0p, ure_p)) % ure_p;

    c2.data.x.PowModN(k1p, ure_p);
    c2.data.y.PowModN(k1p, ure_p);
}

ur_ciphertext ure_encrypt(ElGamal::CryptoProvider const& crypto, BigInteger const& plaintext)
{
    return std::make_pair(crypto.EncryptInteger(plaintext), crypto.GetEncryptedOne());
}

BigInteger ure_decrypt(ElGamal::CryptoProvider const& crypto, ur_ciphertext const& ciphertext)
{
    return crypto.DecryptInteger(ciphertext.first);
}

