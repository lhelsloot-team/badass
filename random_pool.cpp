#include "random_pool.hpp"
#include <random>
#include "shamir.hpp"

random_generator::random_generator()
{
    fill_pool();
}

void random_generator::fill_pool()
{
    for (size_t i = 0; i < random_pool_size; i++)
    {
        random_pool[i] = dev();
    }
}

random_generator::result_type random_generator::operator() ()
{
    static size_t i = 0;
    return random_pool[(i++) % random_pool_size];
}

random_generator::result_type random_generator::get_default()
{
    static size_t i = 0;

    auto r = default_pool[i];

    if (++i == default_pool_size)
    {
        i = 0;
    }

    return r;
}

random_generator::result_type random_pool::get(random_generator::result_type max)
{
    std::uniform_int_distribution<random_generator::result_type> dis(0, max);
    return dis(gen);
}
