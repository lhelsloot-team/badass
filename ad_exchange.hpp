#ifndef BADASS_AD_EXCHANGE_HPP
#define BADASS_AD_EXCHANGE_HPP

#include <unordered_map>
#include "dsp.hpp"
#include "user.hpp"
#include "bid.hpp"

class psp;

class share_converter
{
public:
    void share_profile(user_info const& user, std::vector<shamir::value_t> const& additive_shares, dsp_group const& dsps);
};

class ad_exchange : public share_converter
{
public:
    ad_exchange(std::shared_ptr<psp> psp_arg);

    void add_dsp_group(std::shared_ptr<dsp_group> dsp_group);

    bid_response request_ad(user_info const& user);

    void submit_bid(shamir::index_t r_a, bid_response const& bid);

    std::unordered_map<unsigned long, std::shared_ptr<dsp_group>> dsp_groups;

private:
    std::shared_ptr<psp> m_psp;
    unsigned long next_bid_id = 0;
};

#endif
