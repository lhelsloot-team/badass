
#include <utils/config.h>
#include "timing.hpp"
#include "dataparser_avazu.hpp"
#include "psp.hpp"
#include "ad_exchange.hpp"
#include "logging.hpp"

using namespace std::chrono;
using namespace SeComLib::Utils;

namespace ahead
{

std::array<std::string, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::names{};
std::array<nanoseconds, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::elapsed{};
std::array<unsigned, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::count{};

static Timer<timer_id::update_profile_user, true> upd_timer("update_profile_user");
static Timer<timer_id::update_profile_total, true> upd_timer_total("update_profile_total");
static Timer<timer_id::request_ad_total, true> request_timer_total("request_ad_total");
static Timer<timer_id::update_model_total, true> model_timer_total("update_model_total");

BaseTimer::BaseTimer(timer_id id, std::string&& name)
{
    names[static_cast<unsigned>(id)] = std::move(name);
}

void BaseTimer::update(timer_id id, nanoseconds duration, unsigned reps)
{
    unsigned index = static_cast<unsigned>(id);
    elapsed[index] += duration;
    count[index] += reps;
    //Logger::get().push(names[index], duration.count());
}

void BaseTimer::print_timers()
{
    for (unsigned i = 0; i < elapsed.size(); i++)
    {
        std::cout << names[i] << " averaged " << static_cast<double>(elapsed[i].count()) / (1e6 * count[i]) << " ms over " << count[i] << " runs." << std::endl;
    }
}

void BaseTimer::write_timers()
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::stringstream name;
    name << "time_"
    << Config::GetInstance().GetParameter<unsigned>("ShamirAds.profile_bits")
    << "bits_"
    << Config::GetInstance().GetParameter<unsigned>("ShamirAds.n_dsp_groups")
    << "dsps_"
    << Config::GetInstance().GetParameter<unsigned>("ShamirAds.campaigns_per_dsp")
    << "campaigns_"
    << std::put_time(&tm, "%Y%m%d_%H%M%S")
    << ".log";

    std::ofstream f(name.str());
    f.exceptions(std::ofstream::failbit | std::ofstream::badbit);

    // Write names
    std::copy(names.begin(), names.end() - 1, std::ostream_iterator<std::string>(f, ", "));
    f << names.back() << std::endl;

    // Write averages
    f << static_cast<double>(elapsed[0].count()) / (1e6 * count[0]);
    for (unsigned i = 1; i < elapsed.size(); i++)
    {
        auto avg = static_cast<double>(elapsed[i].count()) / (1e6 * count[i]);
        f << ", " << avg;
    }
    f << std::endl;
}

void timing_main()
{
    auto m_psp = std::make_shared<psp>();
    auto adx = std::make_shared<ad_exchange>(m_psp);

    //auto key_share = m_psp->generate_key_shares();
    //std::shared_ptr<ahead::ThresholdPaillier> dsp_crypto = std::make_shared<ahead::ThresholdPaillier>(m_psp->get_public_key(), key_share);

    user_factory users;

    // Create DSP groups
    auto shamir = runtime::get().shamir;
    unsigned campaign_id = 1;
    for (size_t i = 0; i < runtime::get().n_dsp_groups; i++)
    {
        auto group = std::make_shared<dsp_group>(shamir, m_psp, adx, i);
        group->make_parties<dsp>();
        for (auto& party : group->parties)
        {
            auto d = std::dynamic_pointer_cast<dsp>(party);
            d->set_group(group);
            for (size_t i = 0; i < runtime::get().n_campaigns_per_dsp; i++)
            {
                d->add_campaign(campaign(campaign_id, campaign_id, 1, 1, 0.1));
                campaign_id++;
            }
        }
        adx->add_dsp_group(group);
    }

    // Let's request an ad!
    std::cout << "Opening data reader" << std::endl;
    ahead::DataReader reader(runtime::get().training_data, std::unique_ptr<ahead::AvazuDataParser>(new ahead::AvazuDataParser()));


    unsigned lines = 0;
    ahead::DataLine data;
    while (lines < runtime::get().training_samples && reader.get_line(data))
    {
        lines++;

        upd_timer_total.start();
        upd_timer.start();

        user u = users.get_user(data);

        upd_timer.stop();

        for (auto elem : adx->dsp_groups)
        {
            auto shares = u.share();
            adx->share_profile(u.info, shares.first, *(elem.second));
            m_psp->share_profile(u.info, shares.second, *(elem.second));
        }

        upd_timer_total.stop();


        request_timer_total.start();
        bid_response result = adx->request_ad(u.info);
        request_timer_total.stop();

        //std::cout << "Bid id: " << result.bid_id << std::endl;
#ifndef NO_CRYPTO
        BigInteger dec_ad = ure_decrypt(*u.info.crypto_provider, result.enc_ad);
#endif
        //BigInteger dec_ad = u.info.crypto_provider->DecryptInteger(result.enc_ad);
        //BigInteger group_id = dec_ad & 0xFF;
        //dec_ad >>= 8;
        //std::cout << "Group:  " << group_id.ToString(10) << std::endl;
        //std::cout << "Ad:     " << dec_ad.ToString(10) << std::endl;

        model_timer_total.start();
        auto dsp_group = adx->dsp_groups.at(result.group_id);
        dsp_group->report_response(result.bid_id, u.info, data.label, result.k);
        model_timer_total.stop();

        for (auto& elem : adx->dsp_groups)
        {
            for (auto& d : elem.second->parties)
            {
                std::static_pointer_cast<dsp>(d)->clear_data();
            }
        }

#ifdef PRINT_LOGLOSS
        if (lines % 10 == 0)
        {
            for (auto& losses : m_psp->logloss)
            {
                unsigned k = losses.first;
                double loss = losses.second;
                unsigned count = m_psp->count[k];

                std::cout << "Logloss of " << k << " after " << count << " wins: " << loss / count << std::endl;
            }
        }
#else
        //if (lines % 10 == 0)
        //{
        //    std::cout << "Encountered " << lines << " samples\r" << std::flush;
        //}
#endif

#ifdef MEASURE_RUNTIME
        //Logger::get().flush();

        if (lines % 10 == 0)
        {
            BaseTimer::print_timers();
        }
#endif
    }

    lines = 0;
    double loss = 0;
    while (lines < runtime::get().testing_samples && reader.get_line(data))
    {
        lines++;

        user u = users.get_user(data);

        for (auto elem : adx->dsp_groups)
        {
            auto shares = u.share();
            adx->share_profile(u.info, shares.first, *(elem.second));
            m_psp->share_profile(u.info, shares.second, *(elem.second));
        }

        bid_response result = adx->request_ad(u.info);

        //std::cout << "Bid id: " << result.bid_id << std::endl;
        BigInteger dec_ad = ure_decrypt(*u.info.crypto_provider, result.enc_ad);
        //BigInteger dec_ad = u.info.crypto_provider->DecryptInteger(result.enc_ad);
        //BigInteger group_id = dec_ad & 0xFF;
        //dec_ad >>= 8;
        //std::cout << "Group:  " << group_id.ToString(10) << std::endl;
        //std::cout << "Ad:     " << dec_ad.ToString(10) << std::endl;

        auto dsp_group = adx->dsp_groups.at(result.group_id);
        //dsp_group->report_response(result.bid_id, u.info, data.label, result.enc_k);
        loss -= dsp_group->get_logloss(result.bid_id, data.label);

        if (lines % 100 == 0)
        {
            std::cout << "Logloss after " << lines << " samples: " << loss / lines << std::endl;
        }
    }

    BaseTimer::write_timers();
}

}
