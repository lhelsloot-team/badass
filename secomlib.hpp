#ifndef BADASS_SECOMLIB_HPP
#define BADASS_SECOMLIB_HPP

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"

#include <core/random_provider.h>
#include <core/el_gamal.h>
#include <utils/config.h>

using namespace SeComLib::Core;
using namespace SeComLib::Utils;

#pragma GCC diagnostic pop

#endif
