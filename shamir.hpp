#ifndef BADASS_SHAMIR_HPP
#define BADASS_SHAMIR_HPP

#include <vector>
#include <deque>
#include <numeric>
#include <functional>
#include <type_traits>
#include "random_pool.hpp"

#include "secomlib.hpp"

namespace shamir
{
    typedef uint32_t index_t;
    typedef int32_t numeric_value_t;
    typedef numeric_value_t value_t;

    const uint32_t p = 2147483647;

    inline value_t printable(numeric_value_t v) { return v; }
    inline std::string printable(BigInteger const& v) { return v.ToString(10); }

    //template<typename T>
    //value_t value(T const& v) { return BigInteger(v); }

    numeric_value_t mod(int64_t a, numeric_value_t b);
    BigInteger mod(BigInteger const& a, BigInteger const& b);

    inline value_t to_value(numeric_value_t v)
    {
        return value_t(v);
    }

    template<typename T>
    value_t to_value(T const& v, std::true_type)
    {
        return v.ToUnsignedLong();
    }

    template<typename T>
    value_t to_value(T const& v, std::false_type)
    {
        return v;
    }

    inline value_t to_value(BigInteger const& v)
    {
        return to_value(v, std::is_integral<value_t>());
    }

    inline numeric_value_t to_number(BigInteger const& v)
    {
        return v.ToUnsignedLong();
    }

    inline numeric_value_t to_number(numeric_value_t v)
    {
        return v;
    }

    inline unsigned get_bit(numeric_value_t v, unsigned bit)
    {
        //return BigInteger(v).GetBit(bit); //(v >> bit) & 1;
        return (v >> bit) & 1;
    }

    inline unsigned get_bit(BigInteger const& v, unsigned bit)
    {
        return v.GetBit(bit);
    }

    template<typename T1, typename T2>
    T1& op_add_assign(T1& lhs, T2 const& rhs)
    {
        lhs += rhs;
        return lhs;
    }

    template<typename T1, typename T2>
    T1& op_sub_assign(T1& lhs, T2 const& rhs)
    {
        lhs -= rhs;
        return lhs;
    }

    template<typename T1, typename T2>
    T1& op_mul_assign(T1& lhs, T2 const& rhs)
    {
        lhs *= rhs;
        return lhs;
    }

    template<typename T1, typename T2>
    T1& op_xor_assign(T1& lhs, T2 const& rhs)
    {
        lhs ^= rhs;
        return lhs;
    }

    template<typename T1, typename T2>
    T1& op_div_assign(T1& lhs, T2 const& rhs)
    {
        lhs /= rhs;
        return lhs;
    }

    template<typename T1, typename T2>
    T1 op_xor(T1 const& lhs, T2 const& rhs)
    {
        return (lhs + rhs) - ((lhs * rhs) * 2);
    }

    class shamir;
    typedef std::shared_ptr<shamir> ptr;

    class group;

    class share_t
    {
    public:
        share_t() : y(0) {}
        share_t(value_t const& y_arg);

        template<typename T> share_t operator+ (T const& other) const { return this->transform(other, std::plus<int64_t>()); }
        template<typename T> share_t operator- (T const& other) const { return this->transform(other, std::minus<int64_t>()); };
        template<typename T> share_t operator* (T const& other) const { return this->transform(other, std::multiplies<int64_t>()); };
        template<typename T> share_t operator^ (T const& other) const { return this->transform(other, op_xor<int64_t, int64_t>); }

        template<typename T> share_t& operator+= (T const& other) { return this->transform_assign(other, std::plus<int64_t>()); }
        template<typename T> share_t& operator-= (T const& other) { return this->transform_assign(other, std::minus<int64_t>()); }
        template<typename T> share_t& operator*= (T const& other) { return this->transform_assign(other, std::multiplies<int64_t>()); }
        template<typename T> share_t& operator^= (T const& other) { return this->transform_assign(other, op_xor<int64_t, int64_t>); }
        template<typename T> share_t& operator/= (T const& other) { return this->transform_assign(other, std::divides<int64_t>()); }

        value_t y;

    protected:
        void check_transform(share_t const& other) const;

        template<typename T>
        share_t transform(share_t const& other, T op) const
        {
            //this->check_transform(other);
            return this->transform(other.y, op);
        }

        template<typename T>
        share_t transform(value_t const& other, T op) const
        {
            return share_t(mod(op(static_cast<int64_t>(this->y), other), p));
        }

        template<typename T>
        share_t& transform_assign(share_t const& other, T op)
        {
            //this->check_transform(other);
            return this->transform_assign(other.y, op);
        }

        template<typename T>
        share_t& transform_assign(value_t const& other, T op)
        {
            this->y = mod(op(static_cast<int64_t>(this->y), other), p);
            //assert(this->y > 0 && this->y < this->p);
            return *this;
        }
    } __attribute__((packed));

    typedef std::array<share_t, 5> shares_t;
    typedef std::vector<share_t> share_vector;

    share_vector& operator+= (share_vector& lhs, share_vector const& rhs);
    share_vector& operator-= (share_vector& lhs, share_vector const& rhs);

    class polynomial_t
    {
    public:
        polynomial_t(shamir* s_arg);
        //polynomial_t(shamir const* s_arg, value_t const& v);

        void generate(value_t const& v);

        share_t operator() (index_t x) const;

    protected:
        shamir* s;
        std::vector<value_t> poly;
    };


    class shamir
    {
    public:
        shamir (index_t t_arg, index_t n_arg, size_t l_arg, size_t k_arg);

        shares_t split(value_t const& v);

        value_t combine(shares_t const& shares) const
        {
            return combine(shares, this->t);
        }

        value_t combine(shares_t const& shares, index_t n_shares) const;

        shares_t reduce_degree_split(share_t const& share) { return split(share.y); }
        share_t reduce_degree_combine(shares_t& shares) const;
        share_t reduce_degree_combine2(shares_t const& shares) const;
        value_t randomizer();

        value_t sqrt(value_t const& value) const;

        index_t t, n;
        size_t l, k;
        //value_t p;
        //BigInteger p_bigint;
        index_t multiply_threshold;
//        std::random_device random_dev;
//        std::function<value_t()> randomizer;

        random_pool random;

    private:
        polynomial_t poly_cache;
    };

    class party
    {
        friend class group;

    public:
        party (std::shared_ptr<shamir> s_arg, index_t x_arg);
        virtual ~party() = default;


        /// Check that sufficient operands are available on the stack
        void check_stack(size_t min_size) const
        {
            /*if (this->stack.size() < min_size)
            {
                throw std::invalid_argument("Insufficient operands provided to shamir_party operation");
            }*/
        }

        /// Clear the temp_stack
        void clear_temp_stack() { this->temp_stack.clear(); }

        /// Move a value from the stack to the temp stack
        void move_to_temp_stack() { this->temp_stack.push_front(this->pop_top()); }

        void copy_from_temp_stack(size_t const& i) { this->push(this->temp_stack.at(i)); }


        /// Push value onto stack
        void push(share_t const& value);
        /// Pop value from stack
        void pop();
        /// Get value from top of stack
        share_t& top();
        /// Pop and return
        share_t pop_top();
        /// Access element in stack
        share_t& at(size_t n);
        /// Duplicate value on stack top
        void dup();

        void swap(int const& n1, int const& n2);

        void push_zero() { this->push(share_t(0)); }

        void add() { this->binary_op(op_add_assign<share_t, share_t>); }

        // Add a public constant
        void add(value_t const& value) { this->binary_op(op_add_assign<share_t, value_t>, value); }

        void subtract() { this->binary_op(op_sub_assign<share_t, share_t>); }
        void subtract(value_t const& value) { this->binary_op(op_sub_assign<share_t, value_t>, value); }

        /// Perform multiplication on local shares, and split result
        void multiply() { this->binary_op(op_mul_assign<share_t, share_t>); }

        /// Multiply with public constant
        void multiply(value_t const& value) { this->binary_op(op_mul_assign<share_t, value_t>, value); }

        void exor(value_t const& value) { this->binary_op(op_xor_assign<share_t, value_t>, value); }

        void divide(value_t const& value) { this->binary_op(op_div_assign<share_t, value_t>, value); }

    protected:

        template<typename T>
        void binary_op(T op)
        {
            share_t rhs = this->pop_top();
            share_t& lhs = this->top();
            op(lhs, rhs);
        }

        template<typename T, typename T2>
        void binary_op(T op, T2 const& rhs)
        {
            op(this->top(), rhs);
        }

        void gte_calculate_z();
        void gte_calculate_e(value_t const& c);
        bool gte_can_multiply();
        void gte_multiply();
        void gte_multiply_push_result();
        void gte_adjust(value_t const& c_mod2l, value_t const& inv2l);

        std::shared_ptr<shamir> s;
        std::vector<share_t> stack;
        std::deque<share_t> temp_stack;

        index_t const x;
    };

    class group
    {
    public:
        group (std::shared_ptr<shamir> s_arg);
        void add_party(std::shared_ptr<party> party);

        template<typename T>
        void make_parties(size_t n = 0)
        {
            if (n == 0)
            {
                n = this->s->n;
            }

            for (size_t i = 0; i < n; i++)
            {
                this->parties.push_back(std::make_shared<T>(this->s, this->parties.size() + 1));
            }
        }

        void split(value_t const& value);

        value_t combine()
        {
            return combine(this->s->t);
        }

        value_t combine(index_t n_shares);

        void reduce_degree();
        void reshare(group& destination);
        shares_t reshare(group& destination, shares_t const& shares);

        void add() { this->call(&party::add); }
        void add(value_t const& value) { this->call(&party::add, value); }
        void subtract() { this->call(&party::subtract); }
        void subtract(value_t const& value) { this->call(&party::subtract, value); }
        void multiply();
        void multiply(value_t const& value) { this->call(&party::multiply, value); }
        void multiply_raw() { this->call(&party::multiply); }
        void exor(value_t const& value) { this->call(&party::exor, value); }
        void divide(value_t const& value) { this->call(&party::divide, value); }

        void random_element();
        void random_nonzero_element();
        void random_bit();

        /// Generate k + m random bits, and calculate r'' = int(bits) and
        /// r' = int(bits) % 2^m
        /// This function replaces the temp stack contents by m generated bits,
        /// And pushes r' and r to the stack.
        /// After calling this function, the stack top is [ r r' ...]
        void random_m(size_t m);

        void greater_than_equal();

        void truncate(size_t m);

        void move_to_temp_stack() { this->call(&party::move_to_temp_stack); }

        void inspect(std::string const& name)
        {
            this->call(&party::dup);
            std::cout << name << ": " << printable(this->combine()) << std::endl;
        }

        std::vector<std::shared_ptr<party>> parties;

    protected:
        void check_group_size();
        void preproc();

        template<typename F, typename... Args>
        void call(F const& fun, Args&&... args)
        {
            for (auto& party : this->parties)
            {
                fun(*party, std::forward<Args>(args)...);
            }
        }

        template<typename... Args>
        void call(void (party::*fun)(typename std::decay<Args>::type const&...), Args&&... args)
        {
            for (auto& party : this->parties)
            {
                (*party.*fun)(std::forward<Args>(args)...);
            }
        }

        template<typename T, typename... Args>
        void call_derived(void (T::*fun)(typename std::decay<Args>::type const&...), Args&&... args)
        {
            for (auto& party : this->parties)
            {
                (*std::dynamic_pointer_cast<T>(party).*fun)(std::forward<Args>(args)...);
            }
        }

        std::shared_ptr<shamir> s;
        value_t twoinv, twolinv;
    };
}

#endif
