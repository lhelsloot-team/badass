#ifndef BADASS_USER_HPP
#define BADASS_USER_HPP

#include <memory>

#include "secomlib.hpp"
#include "elgamal.hpp"
#include "universal_reencryption.hpp"
#include "shamir.hpp"
#include "fixed_point.hpp"
#include "datareader.hpp"
#include "config.hpp"

class user_info
{
public:
    typedef uint32_t id_t;
    typedef PlainElGamal crypto_t;
    typedef ur_ciphertext ciphertext_t;
    typedef std::shared_ptr<crypto_t::CryptoProvider> crypto_provider_t;

    user_info(id_t id_arg, crypto_provider_t crypto_arg)
        : id(id_arg), crypto_provider(crypto_arg) {}

    id_t id;
    std::shared_ptr<crypto_t::CryptoProvider> crypto_provider;
};

class user
{
public:
    typedef user_info::id_t id_t;
    typedef user_info::crypto_t crypto_t;
    typedef user_info::ciphertext_t ciphertext_t;
    typedef std::pair<std::vector<shamir::value_t>, std::vector<shamir::value_t>> additive_shares_t;

    user(id_t id_arg, user_info::crypto_provider_t crypto_arg)
        : info(id_arg, crypto_arg), x(runtime::get().profile_d) {}

    user_info info;
    std::vector<number_t> x;

    additive_shares_t share() const;
};


class user_profile_share
{
public:
    user_profile_share(user_info const& user);

    bool complete;
    user_info info;
    shamir::share_vector x;
};


class user_factory
{
public:
    user_factory();

    user get_user(ahead::DataLine const& data);

private:
    user_info::crypto_provider_t crypto_provider;
};

#endif
